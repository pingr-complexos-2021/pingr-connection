// External libraries
const request = require("supertest");

// API module
const API = require("../src/api");

describe("The project", () => {
    let api, mockColl, mockDb, mongoClient, stanConn;
    const corsOptions = { origin: "*" };

    const secret = "SUPERSECRET";

    beforeEach(() => {
        userColl = {
            insertOne: jest.fn(),
            findOne: jest.fn(),
            deleteOne: jest.fn(),
            updateOne: jest.fn()
        };

        hashtagsColl = {
            insertOne: jest.fn(),
            findOne: jest.fn(),
            deleteOne: jest.fn(),
            updateOne: jest.fn()
        };

        mockDb = {
            collection: (collName) => {
                switch (collName) {
                    case "users":
                        return userColl;
                    case "hashtags":
                        return hashtagsColl;
                    default:
                        return undefined;
                }
            },
        };

        mongoClient = { db: () => mockDb };

        stanConn = { publish: jest.fn() };

        api = API(corsOptions, { mongoClient, stanConn, secret });
    });

    it("can use Jest", () => {
        expect(true).toBe(true);
    });

    it("can use Supertest", async () => {
        const response = await request(api).get("/");
        expect(response.status).toBe(200);
        expect(response.body).toBe("Hello, World!");
    });

    it("can use CORS", async () => {
        const response = await request(api).get("/");
        const cors_header = response.header["access-control-allow-origin"];
        expect(cors_header).toBe("*");
    });

    it("Handles user follow requests", async () => {
        const body = {
            follower : "user1"
        }
        const response = await request(api).post("/follow/user").send(body);

        expect(response.status.toBe(201));
        expect(usersColl.findOne.toHaveBeenCalled());
        expect(usersColl.insertOne.toHaveBeenCalled());
    });

    it("Handles user unfollow requests", async () => {
        const body = {
            operation : "unfollow",
            follower : "user1",
            followed : "user2"
        }
        const response = await request(api).post("/follow/user").send(body);

        expect(response.status.toBe(200));
        expect(usersColl.deleteOne.toHaveBeenCalled());
    });

    it("Handles user promotion requests", async () => {
        const body = {
            operation : "promote",
            follower : "user1",
            followed : "user2"
        }
        const response = await request(api).post("/follow/user").send(body);

        expect(response.status.toBe(200));
        expect(usersColl.updateOne.toHaveBeenCalled());
    });

    it("Handles user demotion requests", async () => {
        const body = {
            operation : "demote",
            follower : "user1",
            followed : "user2"
        }
        const response = await request(api).post("/follow/user").send(body);

        expect(response.status.toBe(200));
        expect(usersColl.updateOne.toHaveBeenCalled());
    });

    it("Handles hashtag follow requests", async () => {
        const body = {
            operation : "follow",
            follower : "user",
            followed : "hashtag"
        }
        const response = await request(api).post("/follow/hashtag").send(body);

        expect(response.status.toBe(201));
        expect(usersColl.findOne.toHaveBeenCalled());
        expect(usersColl.insertOne.toHaveBeenCalled());
    });

    it("Handles hashtag unfollow requests", async () => {
        const body = {
            operation : "unfollow",
            follower : "user",
            followed : "hashtag"
        }
        const response = await request(api).post("/follow/hashtag").send(body);

        expect(response.status.toBe(200));
        expect(usersColl.deleteOne.toHaveBeenCalled());
    });
});
