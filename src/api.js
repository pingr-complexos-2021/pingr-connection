const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

const { followMiddleware, unfollowMiddleware, updateFollowMiddleware, getFollowListMiddleware, getSpecialFollowListMiddleware } = require("./middlewares/connections.js");
const { mongoMiddlewareFactory, publishMsgMiddlewareFactory} = require("./middlewares/external.js");
const { authorizationMiddlewareFactory } = require("./middlewares/authorization.js");
const { validateFollowMiddleware, validateUnfollowMiddleware, validateUpdateFollowMiddleware } = require("./validators/messageValidators.js");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
    const api = express();
    const follow = express.Router();

    api.use(express.json());
    api.use(cors(corsOptions));

    api.use("/follow", follow);

    const mongoMiddleware = mongoMiddlewareFactory(mongoClient);
    const publishMsgMiddleware = publishMsgMiddlewareFactory(stanConn);
    const authorizationMiddleware = authorizationMiddlewareFactory(secret);

    follow.get("/:user", mongoMiddleware, getFollowListMiddleware);
    follow.get("/special", mongoMiddleware, authorizationMiddleware, getSpecialFollowListMiddleware);

    follow.post("/:target",   mongoMiddleware, authorizationMiddleware, validateFollowMiddleware,       followMiddleware,       publishMsgMiddleware);
    follow.delete("/:target", mongoMiddleware, authorizationMiddleware, validateUnfollowMiddleware,     unfollowMiddleware,     publishMsgMiddleware);
    follow.put("/:target",    mongoMiddleware, authorizationMiddleware, validateUpdateFollowMiddleware, updateFollowMiddleware, publishMsgMiddleware);

    return api;
};
