const jwt = require("jsonwebtoken");

function authorizationMiddlewareFactory(secret) {
    return (req, res, next) => {
        // TODO: checar com o pessoal se esse eh o formato correto
        const auth = req.get("Authentication");
        const token = auth.split(" ")[1];

        const decoded = jwt.verify(token, secret);
        if (decoded && decoded.id) {
            req.uuid = decoded.id;
            next();
        }

        res.status(403);
        res.json({ error: "Failed to authorize user" });
        return;
    }
}

module.exports = { authorizationMiddlewareFactory };
