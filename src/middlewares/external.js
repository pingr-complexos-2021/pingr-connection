function mongoMiddlewareFactory(mongoClient) {
    return (req, res, next) => {
        const db = mongoClient.db("connections");
        if (db == undefined) res.sendStatus(500);
        req.db = db;
        next();
    };
}

function publishMsgMiddlewareFactory(stanConn) {
    return (req, res, next) => {
        stanConn.publish(req.msg.topic, JSON.stringfy(req.msg.body));
        next();
    };
}

module.exports = { mongoMiddlewareFactory, publishMsgMiddlewareFactory };
