async function followMiddleware(req, res, next) {
    const follower = req.uuid;
    const followed = req.params.target;
    const { type } = req.body;

    const collection = req.db.collection(type);
    const follow = await collection.findOne({ follower, followed });

    if (!follow) {
        const entry = { follower, followed };
        if (type == "user") entry.special = false;

        await collection.insertOne(entry);

        req.msg = {
            topic: "friends.follow",
            body: { follower, followed, type }
        };

        entry.type = type;
        res.status(201);
        res.json({ follower, followed, type });
        next();
    } else {
        res.status(406);
        res.json({ error: "Relação já existe" });
    }

    return;
}

async function unfollowMiddleware(req, res, next) {
    const follower = req.uuid;
    const followed = req.params.target;
    const { type } = req.body;

    const collection = req.db.collection(type);
    let { result } = await collection.deleteOne({ follower, followed });

    if (result.ok != 1) {
        res.status(500);
        res.json({ error: "Erro interno :(" });
    } else if (result.n == 0) {
        res.status(404);
        res.json({ error: "Relação não encontrada" });
    } else {
        req.msg = {
            topic: "friends.unfollow",
            body: { follower, followed, type }
        }

        res.status(200);
        res.json({});
        next();
    }

    return;
}

async function updateFollowMiddleware(req, res, next) {
    const followed = req.uuid;
    const follower = req.params.target;
    const { special } = req.body;

    const collection = req.db.collection("user");
    const { matchedCount, modifiedCount } = await collection.updateOne(
        { follower, followed }, { $set: { special } }
    );

    if (matchedCount == 0) {
        res.status(404);
        res.json({ error: "Relação não encontrada" });
    } else if (modifiedCount == 0) {
        res.status(500);
        res.json({ error: "Erro interno :(" });
    } else {
        req.msg = {
            topic: `friends.${follower}`,
            body: { followed, special }
        }

        res.status(200);
        res.json({ follower, followed, special });
        next();
    }

    return;
}

async function getFollowListMiddleware(req, res, next) {
    const user = req.params.user;
    const { type } = req.body;

    // Query deveria sempre retornar apenas uma entrada, por conta do 1 follower, então selecionar
    // apenas o primeiro faz enviarmos apenas a entrada ao invés de um array com apenas uma entrada
    const followList = await req.db.collection(type).aggregate([
        { "$match":  { follower: user } },
        { "$project": { "follower": 1, "followed": 1 } },
        { "$group": { "_id": "$follower", "follows": { "$addToSet": "$followed" } } },
    ]).toArray();

    if (type == "user") {
        const followerList = await req.db.collection(type).aggregate([
            { "$match":  { followed: user } },
            { "$project": { "follower": 1, "followed": 1 } },
            { "$group": { "_id": "$followed", "followers": { "$addToSet": "$follower" } } }
        ]).toArray();

        res.json(
            {
                user: user,
                follows: followList[0] ? followList[0].follows : [],
                followers: followerList[0] ? followerList[0].followers : []
            }
        );
    } else {
        res.json(
            {
                user: user,
                follows: followList[0] ? followList[0].follows : [],
            }
        );
    }

    res.status(200);
    next();
}

async function getSpecialFollowListMiddleware(req, res, next) {
    const user = req.uuid;
    const { type } = req.body;

    const followList = await req.db.collection(type).aggregate([
        { "$match":  { followed: user, special: true } },
        { "$project": { "follower": 1, "followed": 1 } },
        { "$group": { "_id": "$follower", "follows": { "$addToSet": "$followed" } } }
    ]).toArray();

    if (type == "user") {
        const followerList = await req.db.collection(type).aggregate([
            { "$match":  { follower: user, special: true } },
            { "$project": { "follower": 1, "followed": 1 } },
            { "$group": { "_id": "$followed", "followers": { "$addToSet": "$follower" } } }
        ]).toArray();

        res.json(
            {
                user: user,
                follows: followList[0] ? followList[0].follows : [],
                followers: followerList[0] ? followerList[0].followers : []
            }
        );
    } else {
        res.json(
            {
                user: user,
                follows: followList[0] ? followList[0].follows : [],
            }
        );
    }

    res.status(200);
    next();
}

module.exports = { followMiddleware, unfollowMiddleware, updateFollowMiddleware, getFollowListMiddleware, getSpecialFollowListMiddleware };
