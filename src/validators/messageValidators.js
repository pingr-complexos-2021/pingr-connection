function isValidString(string) {
    if (typeof(string) !== "string" || string.trim() === "")
        return false;
    return true;
}

function isValidType(type) {
    if (isValidString(type) && (type === "user" || type === "hashtag"))
        return true;
    return false;
}

function validateFollowMiddleware(req, res, next) {
    if (!isValidType(req.body.type)) {
        res.status(400);
        res.json({ error: `campo type inválido`});
        return;
    }
    next();
}

function validateUnfollowMiddleware(req, res, next) {
    if (!isValidType(req.body.type)) {
        res.status(400);
        res.json({ error: `campo type inválido`});
        return;
    }
    next();
}

function validateUpdateFollowMiddleware(req, res, next) {
    if (typeof(req.body.special) !== "boolean") {
        res.status(400);
        res.json({ error: `campo special inválido`});
        return;
    }
    next();
}

module.exports = { validateFollowMiddleware, validateUnfollowMiddleware, validateUpdateFollowMiddleware };
